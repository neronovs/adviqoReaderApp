package com.adviqo.readerapp;

import android.app.Application;
import android.support.annotation.NonNull;

public class ApplicationClass extends Application {

    private static ApplicationClass sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }

    @NonNull
    public static ApplicationClass getAppContext() {
        return sInstance;
    }
}
