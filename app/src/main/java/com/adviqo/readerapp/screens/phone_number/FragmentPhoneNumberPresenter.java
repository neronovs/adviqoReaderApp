package com.adviqo.readerapp.screens.phone_number;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Gravity;
import android.widget.TableRow;
import android.widget.TextView;

import com.adviqo.readerapp.ApplicationClass;
import com.adviqo.readerapp.R;
import com.adviqo.readerapp.model.ModelPhoneNumber;
import com.adviqo.readerapp.network.PhoneNumbersUseCase;
import com.adviqo.readerapp.screens.general.LoadingView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class FragmentPhoneNumberPresenter {

    private final String TAG = getClass().getName();
    private final short MAX_COUNT_PHONE_NUMBER = 6;
    private final FragmentPhoneNumberView fragmentPhoneNumberView;
//    private ArrayList<ModelPhoneNumber> phoneNumbersList;
    private ModelPhoneNumber[] phoneNumbersArray;
    private PhoneNumbersUseCase useCase;
    private LoadingView loadingView;

    FragmentPhoneNumberPresenter(@NonNull FragmentPhoneNumberView FragmentPhoneNumberView,
                                 @NonNull PhoneNumbersUseCase useCase) {
        this.fragmentPhoneNumberView = FragmentPhoneNumberView;
        this.useCase = useCase;
    }

    private void setNumbersToPhoneNumbersList(List<ModelPhoneNumber> _phoneNumbersList) {
        //This is a temparial filling of list (due to absence of API)
        if (_phoneNumbersList == null) {
            _phoneNumbersList = new ArrayList<>();
            _phoneNumbersList.add(new ModelPhoneNumber("+49 30 4766548", true));
            _phoneNumbersList.add(new ModelPhoneNumber("+49 32 6547789", false));
            _phoneNumbersList.add(new ModelPhoneNumber("+49 34 2469878", false));
            _phoneNumbersList.add(new ModelPhoneNumber("+49 32 8331547", false));
            _phoneNumbersList.add(new ModelPhoneNumber("+49 31 9751463", false));
            _phoneNumbersList.add(new ModelPhoneNumber("+49 39 1648430", false));
        }

        Observable.just(_phoneNumbersList)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
//                .take(MAX_COUNT_PHONE_NUMBER)
                .map(list -> list.toArray(new ModelPhoneNumber[MAX_COUNT_PHONE_NUMBER]))
                .subscribe(fragmentPhoneNumberView::showNumbers);
    }

    void savePhoneNumbersToServer(ModelPhoneNumber[] modelPhoneNumbers) {
        /* ToDo
            doOnNext(title -> saveTitle(title))
         */

        Log.i(TAG, " savePhoneNumbersToServer() was called");
    }

    void getPhoneNumbersFromServer() {
        //Here must be API call to get phoneNumbers
//        useCase.phoneNumberUseCase()
//                .doOnSubscribe(fragmentPhoneNumberView::showLoadingIndicator)
//                .doAfterTerminate(fragmentPhoneNumberView::hideLoadingIndicator)
//                .subscribe(this::setNumbersToPhoneNumbersList, throwable -> fragmentPhoneNumberView.showError());

        Log.i(TAG, " getPhoneNumbersFromServer() was called");

        //This is a mock of a phone number list
        setNumbersToPhoneNumbersList(null);
    }

}
