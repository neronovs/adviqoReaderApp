package com.adviqo.readerapp.screens.phone_number;

import com.adviqo.readerapp.model.ModelPhoneNumber;

import java.util.ArrayList;

public interface FragmentPhoneNumberView {

    void buttonClick();

    void showNumbers(ModelPhoneNumber[] phoneNumbers);

    void showError();

    void showLoadingIndicator();

    void hideLoadingIndicator();

}
