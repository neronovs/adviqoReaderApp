package com.adviqo.readerapp.screens.phone_number;

import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.adviqo.readerapp.screens.general.LoadingDialog;
import com.adviqo.readerapp.screens.general.LoadingView;
import com.adviqo.readerapp.ApplicationClass;
import com.adviqo.readerapp.R;
import com.adviqo.readerapp.model.ModelPhoneNumber;
import com.adviqo.readerapp.network.PhoneNumberRequest;
import com.adviqo.readerapp.network.PhoneNumbersUseCase;
import com.adviqo.readerapp.network.RequestPhoneNumber;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class FragmentPhoneNumber
        extends Fragment
        implements FragmentPhoneNumberView {

    private FragmentPhoneNumberPresenter presenter;
    private LoadingView loadingView;
    private TableLayout tablelayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.phone_number_layout, container, false);

        loadingView = LoadingDialog.view(getFragmentManager());
        tablelayout = view.findViewById(R.id.phone_number_tablelayout);

        PhoneNumberRequest phoneNumberRequest = new RequestPhoneNumber();
        presenter = new FragmentPhoneNumberPresenter(this,
                new PhoneNumbersUseCase(phoneNumberRequest));

        //Here is initializer for presenter to get phone numbers
        presenter.getPhoneNumbersFromServer();

        return view;
    }

    @Override
    public void buttonClick() {

    }

    @Override
    public void showNumbers(ModelPhoneNumber[] phoneNumbers) {
        //Preparing for the dp measure
        final float scale = getResources().getDisplayMetrics().density;
        int padding_in_px = (int) (scale + 0.5f);

        if (tablelayout != null)
            tablelayout.removeAllViews();
        //Filling a tablelayout with phone number buttons
        for (int i = 0; i < phoneNumbers.length; i++) {
            ModelPhoneNumber modelPhoneNumber = phoneNumbers[i];

            TableRow tr = new TableRow(ApplicationClass.getAppContext());
            tr.setId(100 + i);
            if (modelPhoneNumber.isActiveNumber()) {
                tr.setBackground(getResources().getDrawable(R.color.darkish_purple));
            } else {
                tr.setBackground(getResources().getDrawable(R.color.pinkish_grey));
            }
            tr.setGravity(Gravity.CENTER);
            tr.setPadding(0, 0, 0, 0);
            tr.setLayoutParams(new TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT
            ));
            tr.setOnClickListener(v -> {
                if (!modelPhoneNumber.isActiveNumber()) {
                    Observable.just(phoneNumbers)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .map(arr -> {
                                for (int j = 0; j < arr.length; j++) {
                                    arr[j].setActiveNumber(false);
                                }
                                modelPhoneNumber.setActiveNumber(true);
                                return arr;
                            })
                            .doOnNext(presenter::savePhoneNumbersToServer)
                            .subscribe(this::showNumbers);
                }
            });

            TextView textView = new TextView(ApplicationClass.getAppContext());
            textView.setId(200 + i);
            textView.setText(modelPhoneNumber.getPhoneNumber());
            textView.setPadding(0, 21 * padding_in_px, 0, 21 * padding_in_px);
            textView.setTextColor(
                    modelPhoneNumber.isActiveNumber() ? Color.WHITE :
                            getResources().getColor(R.color.greyish_brown));
            textView.setTextSize(20);
            textView.setTypeface(Typeface.create("sans-serif",Typeface.NORMAL));
            tr.addView(textView);

            tablelayout.addView(tr);

            tr = new TableRow(ApplicationClass.getAppContext());
            tr.setPadding(0, 0, 0, 11 * padding_in_px);

            tablelayout.addView(tr);
        }
    }

    @Override
    public void showError() {
        Toast.makeText(ApplicationClass.getAppContext(),
                "No phone number to show :(", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoadingIndicator() {
        loadingView.showLoadingIndicator();
    }

    @Override
    public void hideLoadingIndicator() {
        loadingView.hideLoadingIndicator();
    }
}
