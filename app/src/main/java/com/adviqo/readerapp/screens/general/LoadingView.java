package com.adviqo.readerapp.screens.general;

public interface LoadingView {

    void showLoadingIndicator();

    void hideLoadingIndicator();

}
