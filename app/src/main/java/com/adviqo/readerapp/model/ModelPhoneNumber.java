package com.adviqo.readerapp.model;

public class ModelPhoneNumber {

    public ModelPhoneNumber() {};

    public ModelPhoneNumber(String phoneNumber, boolean activeNumber) {
        this.phoneNumber = phoneNumber;
        this.activeNumber = activeNumber;
    }

    //A phone number of a customer
    private String phoneNumber;
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    //Informant that the phone number is active for a customer
    private boolean activeNumber;
    public boolean isActiveNumber() {
        return activeNumber;
    }
    public void setActiveNumber(boolean activeNumber) {
        this.activeNumber = activeNumber;
    }

}
