package com.adviqo.readerapp.network;

import com.adviqo.readerapp.model.ModelPhoneNumber;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

class ResponsePhoneNumber {

    @SerializedName("results")
    private ArrayList<ModelPhoneNumber> mPhoneNumbers;

    ArrayList<ModelPhoneNumber> getPhoneNumbers() {
        if (mPhoneNumbers == null) {
            return new ArrayList<>();
        }
        return mPhoneNumbers;
    }

}
