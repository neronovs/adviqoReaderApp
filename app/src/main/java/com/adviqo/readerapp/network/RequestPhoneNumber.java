package com.adviqo.readerapp.network;

import com.adviqo.readerapp.model.ModelPhoneNumber;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

public class RequestPhoneNumber
        implements PhoneNumberRequest {

    @Override
    public Observable<List<ModelPhoneNumber>> requestPhoneNumber() {
        return ApiFactory.getPhoneNumberService()
                .servicePhoneNumber()
                .map(ResponsePhoneNumber::getPhoneNumbers)
                .flatMap(Observable::from)
                .toList();
    }

}
