package com.adviqo.readerapp.network;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

public interface PhoneNumberService {

    @GET("")
    Observable<ResponsePhoneNumber> servicePhoneNumber();

}
