package com.adviqo.readerapp.network;

import com.adviqo.readerapp.model.ModelPhoneNumber;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

public class PhoneNumbersUseCase {

    private final PhoneNumberRequest mRequest;

    public PhoneNumbersUseCase(PhoneNumberRequest request) {
        mRequest = request;
    }

    public Observable<List<ModelPhoneNumber>> phoneNumberUseCase() {
        return mRequest.requestPhoneNumber();
    }
}


