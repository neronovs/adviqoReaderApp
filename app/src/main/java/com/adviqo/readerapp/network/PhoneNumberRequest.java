package com.adviqo.readerapp.network;

import com.adviqo.readerapp.model.ModelPhoneNumber;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

public interface PhoneNumberRequest {

    Observable<List<ModelPhoneNumber>> requestPhoneNumber();

}
